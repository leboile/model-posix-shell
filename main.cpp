#include <iostream>
#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/param.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <signal.h>
#define maxComL 100
//#define green "\e[0;32m"
//#define white "\e[0;37m"
using namespace std;
//int fw=open("/tmp/shell.tmp",O_WRONLY | O_CREAT,0666);
int fw=1;
int fr=open("/dev/null",O_RDONLY);
int currPid=0;
int shellPid=getpid();
void exitHandler(int c)
{

if (currPid==0)
{
 close(fw);
 close(fr);
 unlink("/tmp/shell.tmp");
 exit(0);
}
else
 {
 kill(currPid,SIGINT);
 }
}
void usrHandler(int c)
{
  wait(NULL);
  signal(SIGUSR1,SIG_DFL);
}
void print(const char* s)
{
 cout.clear();
 cout.flush();
 cout<<s;
 cout.flush();
}

int launch(char** commandLine,int count, int ifd, int ofd,int bgMode)
{
int status=-1;
if (!strcmp(commandLine[0],"cd"))
 return chdir(commandLine[1]);
if (!strcmp(commandLine[0],"env"))
{
  cout<<getenv("PATH")<<endl;
  return 1;
}

if (!strcmp(commandLine[0],"addenv"))
{
  if (commandLine[1]==NULL)
    return 1;
  char* s=new char[strlen(getenv("PATH")) + strlen(commandLine[1])+2];
  strcat(s,commandLine[1]);
  s[strlen(commandLine[1])]=':';
  strcat(s,getenv("PATH"));
  setenv("PATH",s,1);
  return 1;
}
if(!strcmp(commandLine[0],"setenv"))
{
  if (commandLine[1]==NULL)
   return 1;
  setenv("PATH",commandLine[1],1);
  return 1;
}
  //cout<<"D: "<<commandLine[0]<<endl;
int a=fork();
if (a<0)
 {
  perror("Fork error in launch: ");
  return -1;
 }
else
if(a==0)
{
 if (ifd>2)
  {
   dup2(ifd,0);
   close(ifd);
  }
 if (ofd>2)
  {
   dup2(ofd,1);
   close(ofd);
  }
 if (bgMode)
 {
  //print("background");
  setpgid(0,0);
  a=fork();
  if (a<0)
  {
   perror("Fork error in launch: ");
   return -1;
  }
  if(a==0)
  {
   if (ifd<2)
   {
    ifd=fr;
    dup2(ifd,0);
   }
   if (ofd<2)
   {
    ofd=fw;
    dup2(ofd,1);
   }
   dup2(fw,2);
   execvp(commandLine[0],commandLine);
   perror("Exec error in launch: ");
   exit(1);
  }
  waitpid(a,NULL,0);
  kill(shellPid,SIGUSR1);
  cout<<"Ended: ["<<a<<']'<<endl;
  exit(1);
 }
 else
 {
  execvp(commandLine[0],commandLine);
  perror("Exec error in launch: ");
 }
 exit(1);
}
  currPid=a;
  if (!bgMode)
  {
    waitpid(a,&status,0);
    if (ifd>2)
     close(ifd);
    if (ofd>2)
     close(ofd);
  }
  else
  {
    signal(SIGUSR1,usrHandler);
    //cout<<"SIGSET"<<endl;
  }
 currPid=0;
 if (WIFEXITED(status))
  return !WEXITSTATUS(status);
 else return -1;
}

void setDefault(char**& args,int& wordsCount,int& k,char& c,char*& outfile, char*& infile, bool& isinput, bool& output,int* p,int& bgMode)
{
for(int i=0; i<wordsCount; i++)
{
  //if(args[i]!=NULL)
 //  cout<<"args: "<<args[i];
 // else cout<<i<<" is a NULL";
 //  cout<<endl;
 delete[] args[i];
}
delete[] args;
k=-1;
c=' ';
args=new char*[1];
args[0]=NULL;
wordsCount=1;
outfile=NULL;
infile=NULL;
isinput=false;
output=false;
bgMode=0;
p[0]=-1;
p[1]=-1;
}

void launcher(char** args,int& lastRes,int& in,int& wordsCount,char* infile,char* outfile,int bgMode)
{
  if (wordsCount>1)
  {
   int f=open(infile,O_RDONLY);
   if (f<0)
   {
    f=in;
    if (infile!=NULL)
    cout<<"Input file error"<<endl;
   }
   int o=open(outfile,O_WRONLY | O_CREAT | O_TRUNC,0777);
   if (o<0 && outfile!=NULL)
    cout<<"Output file error"<<endl;
   if(lastRes)
     lastRes=launch(args,wordsCount,f,o,bgMode);
  }
}
int main()
{
char** args=new char*[1];
args[0]=NULL;
int wordsCount=1;
char input[maxComL];
char c=' ';
char wd[MAXPATHLEN];
char* outfile=NULL;
char* infile=NULL;
int k=-1;
char** back;
bool isC=false;
bool isinput=false;
bool output=false;
int isDelim=0;
int p[2];
int in=0;
int out=1;
int lastRes=1;
int ampCount=0;
int bgMode=0;
p[0]=-1;
p[1]=-1;
signal(SIGINT,exitHandler);
signal(SIGTERM,exitHandler);
while(1)
{
for(int i=0;i<maxComL; i++)
    input[i]='\0';
getcwd(wd,MAXPATHLEN);
//print(wd);
//print("\n");
//print(">");
cout<<"[ "<<wd<<" ]"<<endl;
cout<<">";
while(c!='\n')
{
 c=cin.get();
 switch (c)
 {
  case '&':
   ampCount++;
  case '<':
  case '>':
  case ' ':
  case '\n':
  case '|':
  if (isDelim)
  {
   launcher(args,lastRes,in,wordsCount,infile,outfile,bgMode);
   lastRes=!lastRes;
   setDefault(args,wordsCount,k,c,outfile,infile,isinput,output,p,bgMode);
   in=0;
   out=1;
   isDelim=0;
  }
  if(isC)
  {
   k++;
   input[k]=c;
   break;
  }
  if(k!=-1 && !output && !isinput)
  {
    back=new char*[wordsCount];
    memcpy(back,args,wordsCount*sizeof(char*));
    delete[] args;
    wordsCount++;
    args=new char*[wordsCount];
    memcpy(args,back,(wordsCount-1)*sizeof(char*));
    args[wordsCount-2]=new char[maxComL];
    memcpy(args[wordsCount-2],input,maxComL);
    args[wordsCount-1]=NULL;
    delete[] back;
  }
  else
  if (output && k!=-1)
  {
   outfile=new char[wordsCount];
   strcpy(outfile,input);
   output=false;
  }
  else
  if (isinput && k!=-1)
  {
   infile=new char[wordsCount];
   strcpy(infile,input);
   isinput=false;
  }
  if (c=='>')
   output=true;
  if (c=='<')
   isinput=true;
  if (c=='|')
  {
   isDelim++;
  }
  if (c=='&' && ampCount==2)
   {
   launcher(args,lastRes,in,wordsCount,infile,outfile,bgMode);
   setDefault(args,wordsCount,k,c,outfile,infile,isinput,output,p,bgMode);
   in=0;
   out=1;
   ampCount=0;
   } 
   else 
   {
     if(c=='\n' && ampCount==1)
       bgMode=1;
     if (c!='&') ampCount=0;
   }
  for(int i=0;i<maxComL; i++)
   input[i]='\0';
  k=-1; 
  break;
  case '\"':
   isC=!isC;
  break;
  case EOF:
   goto EXIT;
  break;
  default:
  {
   k++;
   input[k]=c;
   if (isDelim==1)
   {
    pipe(p);
    out=p[1];
    launch(args,wordsCount,in,out,bgMode);
    in=p[0];
    close(p[1]);
    setDefault(args,wordsCount,k,c,outfile,infile,isinput,output,p,bgMode);
	k=0;
   }
   isDelim=0;
  }
 }
}
//if (infile!=NULL)
 //cout<<infile<<endl;
//if (outfile!=NULL)
// cout<<outfile<<endl;
if (lastRes)
 launcher(args,lastRes,in,wordsCount,infile,outfile,bgMode);

lastRes=1;
setDefault(args,wordsCount,k,c,outfile,infile,isinput,output,p,bgMode);
in=0;
out=1;
}
EXIT:
close(fw);
close(fr);
unlink("/tmp/shell.tmp");
return 0;
}

