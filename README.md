The following was done under modern CYGWIN with g++ 4.9.3

adm@WIN-SRU3FHTSGD2 ~

$ g++ main.cpp -o shell.exe

adm@WIN-SRU3FHTSGD2 ~

$ ./shell.exe

[ /home/adm ]
\>ps

      PID    PPID    PGID     WINPID   TTY         UID    STIME COMMAND

     6788    5272    5272       4816  pty1        1000 23:05:53 /usr/bin/ps

     5272    6408    5272       4540  pty1        1000 23:05:43 /home/adm/shell

     6408    1692    6408       4576  pty1        1000 22:38:50 /usr/bin/bash

     5384    4984    5384       1144  pty0        1000 22:41:19 /usr/bin/bash

     1692       1    1692       1692  ?           1000 22:38:50 /usr/bin/mintty

     4984       1    4984       4984  ?           1000 22:41:19 /usr/bin/mintty

[ /home/adm ]
\>./shell.exe

[ /home/adm ]
\>ps

      PID    PPID    PGID     WINPID   TTY         UID    STIME COMMAND

     5272    6408    5272       4540  pty1        1000 23:05:43 /home/adm/shell

     6408    1692    6408       4576  pty1        1000 22:38:50 /usr/bin/bash

     5384    4984    5384       1144  pty0        1000 22:41:19 /usr/bin/bash

     1692       1    1692       1692  ?           1000 22:38:50 /usr/bin/mintty

     4984       1    4984       4984  ?           1000 22:41:19 /usr/bin/mintty

     1324    5272    5272       5092  pty1        1000 23:06:08 /home/adm/shell

     2008    1324    5272       6448  pty1        1000 23:06:09 /usr/bin/ps

[ /home/adm ]
\>ls

a.exe  main(1).cpp  main.cpp  pme_parser  qmath_3  shell.exe  sql  testing

[ /home/adm ]
\>pwd

/home/adm

[ /home/adm ]
\>cp shell.exe new_shell.exe && rm new_shell.exe

[ /home/adm ]
\>ls

a.exe  main(1).cpp  main.cpp  pme_parser  qmath_3  shell.exe  sql  testing

[ /home/adm ]
\>echo "Foo" > Bar

[ /home/adm ]
\>ls

a.exe  Bar  main(1).cpp  main.cpp  pme_parser  qmath_3  shell.exe  sql  testing
